package spb.etu.belyaev.forexinvesting.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import spb.etu.belyaev.forexinvesting.exceptions.ApiHandleException;
import spb.etu.belyaev.forexinvesting.forexapi.ForexTrendApiHandler;
import spb.etu.belyaev.forexinvesting.vkapi.VkWallPostApiHandler;

public class PostStatisticsToVkWallAction {
    public static void post(Context context) {
        try {
            VkWallPostApiHandler.postToWall(
                    InvestAccountViewService.getMessageForWallPosting(
                            ForexTrendApiHandler.getListOfInvestAccounts()
                    )
            );
            new AlertDialog.Builder(context)
                    .setTitle("Excellent!")
                    .setMessage("Message has been posted on your wall")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } catch (ApiHandleException e) {
            new AlertDialog.Builder(context)
                    .setTitle("Error")
                    .setMessage(e.getMessage())
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
}
