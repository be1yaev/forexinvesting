package spb.etu.belyaev.forexinvesting.view;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import spb.etu.belyaev.forexinvesting.Constants;
import spb.etu.belyaev.forexinvesting.dto.InvestAccountDTO;

public class InvestAccountViewService {
    private static final NumberFormat decimalFormatter = new DecimalFormat("#0.00");

    public static String getHtmlAccountsView(List<InvestAccountDTO> investAccounts) {
        StringBuilder sb = new StringBuilder();

        sb.append(
            "<html style='background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(234,247,255,1) 71%,rgba(224,243,255,1) 100%);'>" +
            "<head><style>" +
                "table {" +
                    "width: 100%;" +
                "}" +
                "table, th, td {" +
                    "border: 1px solid black;border-collapse: collapse;" +
                "}" +
                "th, td {" +
                    "padding: 5px;text-align: center;" +
                "}" +
                ".closed {" +
                    "color: grey;" +
                "}" +
                ".loss {" +
                    "background-color: rgb(255, 224, 229);" +
                "}" +
                ".profit {" +
                    "background-color: rgb(224, 255, 236);" +
                "}" +
                ".summary {" +
                    "background-color: rgb(252, 255, 224);" +
                "}" +
            "</style></head>"
        );

        sb.append(
            "<body><table>" +
            "<tr>" +
                "<th>Sum</th>" +
                "<th>Inc</th>" +
                "<th>Depo</th>" +
                "<th>PP</th>" +
                "<th>FP</th>" +
                "<th>Status</th>" +
                "<th>Wthd</th>" +
            "</tr>"
        );

        double fullAvailSum = 0;
        double fullDepo = 0;
        double fullWithdraws = 0;

        for(InvestAccountDTO account : investAccounts) {
            sb.append(
                "<tr" +
                    ((account.getStatus() == Constants.InvestorsInfoStatus.CLOSED)
                        ? " class=closed"
                        : ((Double.valueOf(account.getPeriodProfitPercent()) > 0)
                            ? " class=profit"
                            : ((Double.valueOf(account.getPeriodProfitPercent()) < 0)
                                ? " class=loss"
                                : ""
                            )
                        )
                    )
                + ">"
            );
            sb.append("<td>" + decimalFormatter.format(account.getAvailableSum()) + "$</td>");
            sb.append("<td>" + decimalFormatter.format(account.getAvailableSum() + account.getWithdraws() - account.getDeposit()) + "$</td>");
            sb.append("<td>" + decimalFormatter.format(account.getDeposit()) + "$</td>");
            sb.append("<td>" + (account.getPeriodProfitPercent().equals(0) ? "" : decimalFormatter.format(account.getPeriodProfitPercent()) + "%") + "</td>");
            sb.append("<td>" + decimalFormatter.format(account.getProfitPercent()) + "%</td>");
            sb.append("<td>" + account.getStatus() + "</td>");
            sb.append("<td>" + decimalFormatter.format(account.getWithdraws()) + "</td>");
            sb.append("</tr>");

            fullAvailSum += account.getAvailableSum();
            fullDepo += account.getDeposit();
            fullWithdraws += account.getWithdraws();
        }
        sb.append(
            "<tr>" +
                "<th style='border-right:0px;'>Summary:</th>" +
                "<th style='border:0px;'></th>" +
                "<th style='border:0px;'></th>" +
                "<th style='border:0px;'></th>" +
                "<th style='border:0px;'></th>" +
                "<th style='border:0px;'></th>" +
                "<th style='border-left:0px;'></th>" +
            "</tr>");
        sb.append(
            "<tr class=summary>" +
                "<td>" + decimalFormatter.format(fullAvailSum) + "$</td>" +
                "<td>" + decimalFormatter.format(fullAvailSum + fullWithdraws - fullDepo) + "$</td>" +
                "<td>" + decimalFormatter.format(fullDepo) + "$</td>" +
                "<td></td>" +
                "<td>" + decimalFormatter.format(100*(fullAvailSum + fullWithdraws)/fullDepo - 100) + "%</td>" +
                "<td></td>" +
                "<td>" + decimalFormatter.format(fullWithdraws) + "$</td>" +
            "</tr>");
        sb.append("</table></body>");

        return sb.toString();
    }

    public static String getMessageForWallPosting(List<InvestAccountDTO> investAccounts) {
        double fullAvailSum = 0;
        double fullDepo = 0;
        double fullWithdraws = 0;

        for(InvestAccountDTO account : investAccounts) {
            fullAvailSum += account.getAvailableSum();
            fullDepo += account.getDeposit();
            fullWithdraws += account.getWithdraws();
        }

        String message = "My Forex statistics:\n\r" +
                "Deposit: " + decimalFormatter.format(fullDepo) + "$\n\r" +
                "Current account balance: " + decimalFormatter.format(fullAvailSum + fullWithdraws) + "$\n\r" +
                "Income: " + decimalFormatter.format(fullAvailSum + fullWithdraws - fullDepo) + "$ " +
                "(" + decimalFormatter.format(100*(fullAvailSum + fullWithdraws)/fullDepo - 100) + "%)\n\r\n\r" +
                "Do you want too? Good Luck: http://vk.com";

        return message;
    }
}
