package spb.etu.belyaev.forexinvesting.vkapi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

import spb.etu.belyaev.forexinvesting.exceptions.ApiHandleException;

public class VkWallPostApiHandler {
    public static void postToWall(String message) throws ApiHandleException {
        if (VKSdk.isLoggedIn()) {
            VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, message));
            request.start();
        } else {
            throw new ApiHandleException("User not logged in VK");
        }
    }
}
