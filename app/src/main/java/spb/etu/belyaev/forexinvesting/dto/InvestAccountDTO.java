package spb.etu.belyaev.forexinvesting.dto;

import spb.etu.belyaev.forexinvesting.Constants;

/**
 * Created by Andrew on 01.04.2015.
 */
public class InvestAccountDTO implements Comparable {
    private String id;
    private Constants.InvestorsInfoStatus status;
    private Double availableSum;
    private Double deposit;
    private Double withdraws;
    private Double profitPercent;
    private Double periodProfitPercent;

    public InvestAccountDTO() {
    }

    public InvestAccountDTO(String id, Constants.InvestorsInfoStatus status, Double availableSum, Double deposit, Double withdraws, Double profitPercent, Double periodProfitPercent) {

        this.id = id;
        this.status = status;
        this.availableSum = availableSum;
        this.deposit = deposit;
        this.withdraws = withdraws;
        this.profitPercent = profitPercent;
        this.periodProfitPercent = periodProfitPercent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Constants.InvestorsInfoStatus getStatus() {
        return status;
    }

    public void setStatus(Constants.InvestorsInfoStatus status) {
        this.status = status;
    }

    public Double getAvailableSum() {
        return availableSum;
    }

    public void setAvailableSum(Double availableSum) {
        this.availableSum = availableSum;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Double getWithdraws() {
        return withdraws;
    }

    public void setWithdraws(Double withdraws) {
        this.withdraws = withdraws;
    }

    public Double getProfitPercent() {
        return profitPercent;
    }

    public void setProfitPercent(Double profitPercent) {
        this.profitPercent = profitPercent;
    }

    public Double getPeriodProfitPercent() {
        return periodProfitPercent;
    }

    public void setPeriodProfitPercent(Double periodProfitPercent) {
        this.periodProfitPercent = periodProfitPercent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvestAccountDTO that = (InvestAccountDTO) o;

        if (availableSum != null ? !availableSum.equals(that.availableSum) : that.availableSum != null)
            return false;
        if (deposit != null ? !deposit.equals(that.deposit) : that.deposit != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (periodProfitPercent != null ? !periodProfitPercent.equals(that.periodProfitPercent) : that.periodProfitPercent != null)
            return false;
        if (profitPercent != null ? !profitPercent.equals(that.profitPercent) : that.profitPercent != null)
            return false;
        if (status != that.status) return false;
        if (withdraws != null ? !withdraws.equals(that.withdraws) : that.withdraws != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (availableSum != null ? availableSum.hashCode() : 0);
        result = 31 * result + (deposit != null ? deposit.hashCode() : 0);
        result = 31 * result + (withdraws != null ? withdraws.hashCode() : 0);
        result = 31 * result + (profitPercent != null ? profitPercent.hashCode() : 0);
        result = 31 * result + (periodProfitPercent != null ? periodProfitPercent.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object another) {
        InvestAccountDTO anotherAccount = (InvestAccountDTO)another;
        if (this.getStatus().getIntValue() != anotherAccount.getStatus().getIntValue()) {
            return (this.getStatus().getIntValue() - anotherAccount.getStatus().getIntValue());
        }

        if (!this.getProfitPercent().equals(anotherAccount.getProfitPercent())) {
            return (-1) * Double.compare(this.getProfitPercent(), anotherAccount.getProfitPercent());
        }

        return Double.compare(this.getDeposit(), anotherAccount.getDeposit());
    }
}
