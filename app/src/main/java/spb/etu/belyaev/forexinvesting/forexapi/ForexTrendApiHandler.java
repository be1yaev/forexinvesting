package spb.etu.belyaev.forexinvesting.forexapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import spb.etu.belyaev.forexinvesting.Constants;
import spb.etu.belyaev.forexinvesting.dto.InvestAccountDTO;
import spb.etu.belyaev.forexinvesting.exceptions.ApiHandleException;

public class ForexTrendApiHandler {
    public static List<InvestAccountDTO> getListOfInvestAccounts() throws ApiHandleException {
        try {
            JSONObject json = HttpPostRequestSender.readJsonFromUrl(Constants.URL_GET_USER_INVESTORS_INFO);
            JSONArray jsonDataArray = json.getJSONArray(Constants.JSON_INVESTORS_INFO_ARRAY_DATA);

            List<InvestAccountDTO> investAccountDTOs = new ArrayList<>();

            for (int i = 0; i < jsonDataArray.length(); i++) {
                JSONObject jsonObject = jsonDataArray.getJSONObject(i);
                InvestAccountDTO accountFromArray = new InvestAccountDTO();
                accountFromArray.setId(
                        jsonObject.getString(Constants.JSON_INVESTORS_INFO_FIELD_ID)
                );
                accountFromArray.setStatus(
                        Constants.InvestorsInfoStatus.getStatusFromStringValue(
                                jsonObject.getString(Constants.JSON_INVESTORS_INFO_FIELD_STATUS)
                        )
                );
                accountFromArray.setAvailableSum(
                        jsonObject.getDouble(Constants.JSON_INVESTORS_INFO_FIELD_AVAILABLE_SUM)
                );
                accountFromArray.setDeposit(
                        jsonObject.getDouble(Constants.JSON_INVESTORS_INFO_FIELD_DEPOSIT)
                );
                accountFromArray.setWithdraws(
                        jsonObject.getDouble(Constants.JSON_INVESTORS_INFO_FIELD_WITHDRAWS)
                );
                accountFromArray.setProfitPercent(
                        jsonObject.getDouble(Constants.JSON_INVESTORS_INFO_FIELD_PROFIT_PERCENT)
                );
                accountFromArray.setPeriodProfitPercent(
                        jsonObject.getDouble(Constants.JSON_INVESTORS_INFO_FIELD_PERIOD_PROFIT_PERCENT)
                );

                investAccountDTOs.add(accountFromArray);
            }

            Collections.sort(investAccountDTOs);
            return investAccountDTOs;
        } catch (IOException | JSONException | InterruptedException | ExecutionException e) {
            throw new ApiHandleException(e);
        }
    }
}
