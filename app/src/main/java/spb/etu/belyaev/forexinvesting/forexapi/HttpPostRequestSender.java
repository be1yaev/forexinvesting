package spb.etu.belyaev.forexinvesting.forexapi;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class HttpPostRequestSender extends AsyncTask<String, Void, String> {
    private static DefaultHttpClient client = new DefaultHttpClient();

    private volatile String errorValue;
    private volatile String responseString;

    @Override
    protected String doInBackground(String... params) {
        String url = params[0];
        try {
            HttpPost postRequest = new HttpPost(url);
            postRequest.addHeader("Authorization", "Basic YmVseWFldjozYzIzNTBhZmIwMjU1NzY0MTViYzdkNDk4ZTc5MjYxOA==");
            postRequest.addHeader("Accept", "application/json, text/json");

            HttpResponse getResponse = client.execute(postRequest);
            final int statusCode = getResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                errorValue = "Error " + statusCode + " for URL " + url;
                this.cancel(false);
            }
            HttpEntity getResponseEntity = getResponse.getEntity();
            if (getResponseEntity != null) {
                return EntityUtils.toString(getResponseEntity);
            }
        } catch (IOException e) {
            errorValue = "Error: " + e.getMessage() + " for URL " + url;
            this.cancel(false);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        responseString = s;
    }

    public String getErrorValue() {
        return errorValue;
    }

    public String getResponseString() {
        return responseString;
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException, ExecutionException, InterruptedException {
        HttpPostRequestSender httpPostRequestSender = new HttpPostRequestSender();
        String jsonText = httpPostRequestSender.execute(url).get();
        JSONObject json = new JSONObject(jsonText);
        return json;
    }
}
