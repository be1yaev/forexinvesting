package spb.etu.belyaev.forexinvesting.exceptions;

/**
 * Created by Andrew on 01.04.2015.
 */
public class ApiHandleException extends Exception {
    public ApiHandleException(Throwable throwable) {
        super(throwable);
    }

    public ApiHandleException() {
    }

    public ApiHandleException(String detailMessage) {
        super(detailMessage);
    }
}
