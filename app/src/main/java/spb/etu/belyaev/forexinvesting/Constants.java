package spb.etu.belyaev.forexinvesting;

import com.vk.sdk.VKScope;

public class Constants {
    public static final String VK_APPLICATION_ID = "4853853";
    
    public static final String URL_GET_USER_INVESTORS_INFO = "http://api.fx-trend.com/info/get_user_investors_info";
    public static final String JSON_INVESTORS_INFO_ARRAY_DATA = "data";
    public static final String JSON_INVESTORS_INFO_FIELD_ID = "investor_id";
    public static final String JSON_INVESTORS_INFO_FIELD_STATUS = "status";
    public static final String JSON_INVESTORS_INFO_FIELD_AVAILABLE_SUM = "availsum";
    public static final String JSON_INVESTORS_INFO_FIELD_DEPOSIT = "investors_deposit";
    public static final String JSON_INVESTORS_INFO_FIELD_WITHDRAWS = "investors_withdraw";
    public static final String JSON_INVESTORS_INFO_FIELD_PROFIT_PERCENT = "profit_percent";
    public static final String JSON_INVESTORS_INFO_FIELD_PERIOD_PROFIT_PERCENT = "period_profit";

    public enum InvestorsInfoStatus {
        NOT_ACTIVE(0), ACTIVATED(1), CLOSED(2), DELETED(3);

        private int intValue;

        InvestorsInfoStatus(int statusIntValue) {
            this.intValue = statusIntValue;
        }

        public int getIntValue() {
            return intValue;
        }

        public static InvestorsInfoStatus getStatusFromStringValue(String statusStringValue) {
            switch(Integer.valueOf(statusStringValue)) {
                case 0:
                    return NOT_ACTIVE;
                case 1:
                    return ACTIVATED;
                case 2:
                    return CLOSED;
                case 3:
                    return DELETED;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }


    public static final String[] sMyScope = new String[] {
            VKScope.FRIENDS,
            VKScope.WALL,
            VKScope.PHOTOS,
            VKScope.NOHTTPS
    };
}
